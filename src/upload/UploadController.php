<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2015 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: yunwuxin <448901948@qq.com>
// +----------------------------------------------------------------------

namespace service\upload;

use service\upload\Upload;
use think\Request;

class UploadController
{
    public function index(Request $request)
    {
        $param = $request->param();
        $type = $param['type']??'local';
        $topic = $param['topic']??'topic';

        $upload = new Upload($type);

        $upload->setTopic($topic);

        $url = $upload->upload($request->file());

        if($url){
            return return_success(['data'=>$url]);
        }

    }
}
