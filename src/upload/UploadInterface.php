<?php
namespace service\upload;

interface UploadInterface
{
    /**
     * 上传
     *
     * @return boolean
     */
    public function upload($file);

    /**
     * 设置上传目录
     *
     * @param string $name
     * @return void
     */
    public function setTopic($name);

}