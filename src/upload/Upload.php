<?php
namespace service\upload;

use service\upload\SmsInterface;

class Upload
{
    //上传服务类型
    protected $type = [
        'local'=>'service\upload\local\Upload',
    ];

    protected $storage = '';

    /**
     * 构造类型
     *
     * @param [type] $type
     * @param string $scene
     */
    public function __construct($type="local")
    {
        $this->storage = new $this->type[$type]();

        if(!$this->storage instanceof UploadInterface){
            return_error(['info'=>'上传组件异常']);
        }
    }

    /**
     * 初始化
     *
     * @param [type] $type
     * @param string $scene
     * @return void
     */
    public function init($type)
    {
        $this->storage = new $this->type[$type]();

        if(!$this->storage instanceof UploadInterface){
            return_error(['info'=>'上传组件异常']);
        }
    }

    /**
     * 上传
     *
     * @param object $file 文件对象
     * @return void
     */
    public function upload($file)
    {
        return $this->storage->upload($file);
    }

    /**
     * 设置上传目录
     *
     * @param string $name
     * @return void
     */
    public function setTopic($name){
        return $this->storage->setTopic($name);
    }

}