<?php
namespace service\yklog;

use think\Request;
use think\Response;

abstract class YkLogAbstract
{
    protected $methodMap;

    /**
     * 获取日志内容
     *
     * @param Request $request
     * @param Response $response
     * @param array $apiConfig
     * @param array $beforeData
     * @param array $afterData
     * @return void
     */
    public function getContent(Request $request,Response $response,$apiConfig,$beforeData,$afterData)
    {
        $api = $request->pathinfo();

        $method = $this->methodMap[$api]??null;

        if($method){
            return $this->$method($request,$response,$apiConfig,$beforeData,$afterData);
        }

        return $this->default($apiConfig);
    }

    /**
     * 默认返回配置中的内容
     *
     * @param array $apiConfig
     * @return void
     */
    protected function default($apiConfig)
    {
        return ['content'=>$apiConfig['content']??''];
    }
}