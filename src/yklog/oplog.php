<?php
use think\facade\Env;

// +----------------------------------------------------------------------
// | 操作日志设置
// +----------------------------------------------------------------------
return [
    'index'=>[
        'appname'=>'司机小程序',
        'operator_type'=>"driver",
        'module'=>[],
        'api'=>[],
    ],
    'admin'=>[
        'appname'=>'管理后台',
        'operator_type'=>"admin",
        'module'=>[
            'order'=>'订单',
            'other'=>'杂项'
        ],
        'lib_path'=>'app\\index\\log',
        'api'=>[
            'statistic/time'=>['title'=>'首页统计','module'=>'other','type'=>4],
            'order/update'=>['title'=>'更新订单','module'=>'other','type'=>4]
        ],
    ]
];