<?php

namespace service;

use service\captcha\facade\Captcha;
use think\Request;
use think\Route;
use think\Service;
use think\Validate;

/**
 * 图形验证码
 */
class CaptchaService extends Service
{
    public function boot(Route $route,Request $request)
    {
        $route->get('captcha/[:token]/[:config]', "\\service\\captcha\\CaptchaController@index");

        Validate::maker(function ($validate) use ($request) {
            $validate->extend('captcha', function ($value) use ($request) {
                return Captcha::check($value,$request->param('captcha_token',''));
            }, ':attribute错误!');
        });
    }
}
