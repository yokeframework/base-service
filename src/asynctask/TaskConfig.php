<?php
namespace service\asynctask;

use app\common\Cron;
use app\common\Logs;

class TaskConfig
{
    protected $task=[
        'cron'=>[Cron::class,'run'],
        'logs'=>[Logs::class,'run'],
    ];

    protected $config = ['domain'=>'http://127.0.0.1:8098'];

    public function __construct()
    {
        $config = config('ykservbase')['asynctask']??null;

        if($config){
            $this->task = $config['task'];
            $this->config = $config['extra'];
        }
    }

    public function get($key){
        return $this->config[$key]??NULL;
    }

    public function task($name){
        return $this->task[$name]??NULL;
    }
}