<?php
namespace service\asynctask;

use base\tools\HttpHelper;

class Helper extends HttpHelper
{
    public static $connectTimeout = 30;//30 second
    public static $readTimeout = 80;//80 second
    public static $isThrow = false;
}