<?php
namespace service;

use app\model\Log;
use service\yklog\YkLogAbstract;
use think\facade\Log as FacadeLog;
use think\Service;

class LogService extends Service
{
    private $beforeData = '';
    private $afterData = '';

    /**
     * 注册服务和中间件
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('yklog',LogService::class);
    }

    /**
     * 设置变动前数据
     *
     * @param array $data
     * @return void
     */
    public function setBeforeData($data){
        $this->beforeData = $data;
    }

    /**
     * 设置变动后的数据
     *
     * @param array $data
     * @return void
     */
    public function setAfterData($data){
        $this->afterData = $data;
    }

    /**
     * 执行日志记录
     *
     * @return void
     */
    public function opLog($response)
    {  

        $method = request()->method();
        if($method!='POST')return true;

        $app = app('http')->getName();
        $api = request()->pathinfo();

        if($api == 'async/task')return true;
        
        $data = [
            'api'=>$api,
            'ip'=>request()->ip(),
            'user_agent'=>request()->header('user_agent'),
            'param'=>json_encode(request()->param(),JSON_UNESCAPED_UNICODE),
            'code'=>$response->getCode(),
            'app'=>$app,
            'before_data'=>is_string($this->beforeData)?$this->beforeData:json_encode($this->beforeData,JSON_UNESCAPED_UNICODE),
            'after_data'=>is_string($this->afterData)?$this->afterData:json_encode($this->afterData,JSON_UNESCAPED_UNICODE)
        ];
        $config = config('oplog')[$app]??'';

        if($config){
            $loginUser = request()->loginUser??null;

            if($loginUser){
                $data['operator_type']=$config['operator_type'];
                $data['operator_id']=$loginUser[$config['operator_type']."_id"];
                $data['operator_name']=$loginUser[$config['operator_type']."_name"];
            }

            $apiConfig = $config['api'][$api]??null;
            if($apiConfig){
                $data['module']=$apiConfig['module'];
                $data['title']=$apiConfig['title'];
                $data['type']=$apiConfig['type'];

                $class = $config['lib_path'].$apiConfig['module'];

                if(class_exists($class)){
                    $lib = new $class();
                    if($lib instanceof YkLogAbstract){
                        $cusData=$lib->getContent(request(),$response,$apiConfig,$this->beforeData,$this->afterData);//定制数据
                        $data = array_merge($data,$cusData);
                        $data['is_display']=1;
                    }
                }

            }
        }

        if(empty($data['content'])){
            $data['content']="请求返回:".json_encode($response->getData(),JSON_UNESCAPED_UNICODE);
        }

        if(empty($data['title'])){
            $data['title']="请求接口:".$data['api'];
        }

        if(empty($data['module']))$data['module']='Other';

        $log = new Log();

        $log->save($data);
    }
    
}