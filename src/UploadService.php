<?php

namespace service;

use think\Request;
use think\Route;
use think\Service;


/**
 * 文件上传服务类
 */
class UploadService extends Service
{   
    /**
     * 服务注册引导
     *
     * @param Route $route
     * @param Request $request
     * @return void
     */
    public function boot(Route $route,Request $request)
    {
        $route->post('upload/[:type]/[:topic]', "\\service\\upload\\UploadController@index");
    }

}